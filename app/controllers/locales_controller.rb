class LocalesController < ApplicationController
  def update
    I18n.default_locale = params[:id]

    redirect_to cars_path
  end
end
