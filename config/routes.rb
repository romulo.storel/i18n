Rails.application.routes.draw do
  root 'cars#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :cars, only: [:index, :show]
  resources :locales, only: :update
end
